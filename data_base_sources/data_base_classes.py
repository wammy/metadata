class Schema:
    """
        Describes data base schema.
    """
    def __init__(self):
        self.description = None
        self.version = None
        self.name = None
        self.tables = []
        self.domains = []
        self.ft_engine = None


class Table:
    """
        Describes table in data base.
    """
    def __init__(self):
        self.name = None
        self.description = None
        self.add = False
        self.edit = False
        self.delete = False
        self.htt_flags = None
        self.acc_lvl = None
        self.fields = []
        self.cnst = []
        self.idxs = []


class Field:
    """
        Describes field in table
    """
    def __init__(self):
        self.name = None
        self.rname = None
        self.domain = None
        self.description = None
        self.input = False
        self.edit = False
        self.show_in_grid = False
        self.is_mean = False
        self.auto_calculated = False
        self.req = False


class Domain:
    """
        Describes domains in data base
    """
    def __init__(self):
        self.name = None
        self.description = None
        self.type = None
        self.align = None
        self.width = None
        self.precision = None
        self.show_null = False
        self.summable = False
        self.case_sense = False
        self.show_lead_nulls = False
        self.sep = False
        self.char_length = None
        self.length = None
        self.scale = None


class Constraint:
    """
        Describes constraint of data base
    """
    def __init__(self):
        self.name = None
        self.kind = None
        self.items = None
        self.ref_type = None
        self.ref = None
        self.value_edit = False
        self.casc_del = False
        self.full_casc_del = False


class Index:
    """
        Describes indices of data base
    """
    def __init__(self):
        self.name = None
        self.ft = False
        self.unique = False
        self.fields = []
