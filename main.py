import xml.dom.minidom as md
import data_base_sources.data_base_classes as dbc

xml = md.parse('xmls/TASKS.xml')
xml2 = md.parse('xmls/PRJADM.xml')
# OK
# TODO: XML -> RAM


def get_indices(xml_param):
    """
    :param      xml_param: source xml file

    :return:    list_of_indices: list of indices in xml_param

    Parse an xml_param file to find indices in.
    """
    list_of_indices = []
    for index in xml_param.getElementsByTagName("index"):
        src = dbc.Index()
        src.fields.append(index.getAttribute("field"))
        # print(src.fields)
        for an, av in index.attributes.items():
            # print(an, av) - вылезает либо field, либо props, field не надо
            if an.lower() == "field":
                pass
            elif an.lower() == "props":
                for prp in av.split(", "):
                    # print(prp) - fulltext, uniqueness
                    if prp == "fulltext":
                        src.ft = True
                    elif prp == "uniqueness":
                        src.unique = True
                    else:
                        # тут ошибку надо добавить, ValueError() по идее . . .
                        print("Bad format")
            else:
                # тут тоже ошибку надо добавить, тоже ValueError() . . .
                print("Bad attribute")
            """
                Ветки else, предназначенные для обработки ошибок, не вылезают.
            """
        list_of_indices.append(src)
    return list_of_indices
